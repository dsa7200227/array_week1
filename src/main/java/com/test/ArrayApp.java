package com.test;

/**
 * Hello world!
 */
public class ArrayApp {
    public static void main(String[] args) {
        long[] arr; // reference to array
        arr = new long[100]; // make array
        int nElems = 0; // number of items
        int j; // loop counter
        long searchKey; // key of item to search for

        // Insert 10 items into the array
        arr[0] = 77;
        arr[1] = 99;
        arr[2] = 44;
        arr[3] = 55;
        arr[4] = 22;
        arr[5] = 88;
        arr[6] = 11;
        arr[7] = 0; // corrected to 0 (not 00)
        arr[8] = 66;
        arr[9] = 33;
        nElems = 10; // update number of elements

        // Display items in the array
        for (j = 0; j < nElems; j++) { // display items
            System.out.print(arr[j] + " ");
        }
        System.out.println();

        // Find item with key 66
        searchKey = 66;
        for (j = 0; j < nElems; j++) { // search loop
            if (arr[j] == searchKey) { // found item?
                break; // yes, exit loop
            }
        }
        if (j == nElems) { // item not found?
            System.out.println("Can't find " + searchKey);
        } else {
            System.out.println("Found " + searchKey);
        }

        // Delete item with key 55
        searchKey = 55;
        for (j = 0; j < nElems; j++) { // search loop
            if (arr[j] == searchKey) { // found item?
                break; // yes, exit loop
            }
        }
        for (int k = j; k < nElems - 1; k++) { // move higher ones down
            arr[k] = arr[k + 1];
        }
        nElems--; // decrement size

        // Display items in the array after deletion
        for (j = 0; j < nElems; j++) { // display items
            System.out.print(arr[j] + " ");
        }
        System.out.println();
    } // end main()
} // end class ArrayApp
