package com.test;

class LowArray {
    private long[] a; // reference to array a

    public LowArray(int size) {
        a = new long[size]; // create array
    }

    public void setElem(int index, long value) {
        a[index] = value; // set value at index
    }

    public long getElem(int index) {
        return a[index]; // get value at index
    }
}

class LowArrayApp {
    public static void main(String[] args) {
        LowArray arr; // reference
        arr = new LowArray(100); // create LowArray object
        int nElems = 0; // number of items in array
        int j; // loop variable

        // Insert 10 items
        arr.setElem(0, 77);
        arr.setElem(1, 99);
        arr.setElem(2, 44);
        arr.setElem(3, 55);
        arr.setElem(4, 22);
        arr.setElem(5, 88);
        arr.setElem(6, 11);
        arr.setElem(7, 00);
        arr.setElem(8, 66);
        arr.setElem(9, 33);
        nElems = 10; // now 10 items in array

        // Display items
        for (j = 0; j < nElems; j++) {
            System.out.print(arr.getElem(j) + " ");
        }
        System.out.println();

        int searchKey = 26; // search for data item

        // Linear search
        for (j = 0; j < nElems; j++) {
            if (arr.getElem(j) == searchKey) { // found item
                break;
            }
        }
        if (j == nElems) {
            System.out.println("Can't find " + searchKey); // not found
        } else {
            System.out.println("Found " + searchKey); // found
        }

        // Delete value 55
        for (j = 0; j < nElems; j++) {
            if (arr.getElem(j) == 55) { // find 55
                break;
            }
        }
        for (int k = j; k < nElems - 1; k++) {
            arr.setElem(k, arr.getElem(k + 1)); // move higher ones down
        }
        nElems--; // decrement size

        // Display items after deletion
        for (j = 0; j < nElems; j++) {
            System.out.print(arr.getElem(j) + " ");
        }
        System.out.println();
    }
}
